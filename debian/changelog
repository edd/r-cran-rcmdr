rcmdr (2.9-5-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 24 Oct 2024 13:15:54 -0500

rcmdr (2.9-4-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 04 Oct 2024 21:47:45 -0500

rcmdr (2.9-2-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 08 Feb 2024 08:02:10 -0600

rcmdr (2.9-1-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 03 Oct 2023 17:19:05 -0500

rcmdr (2.9-0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version
  * debian/control: Switch to virtual debhelper-compat (= 13)

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 24 Aug 2023 09:39:29 -0500

rcmdr (2.8-0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 20 Aug 2022 18:40:00 -0500

rcmdr (2.7-2-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 06 Jan 2022 17:59:39 -0600

rcmdr (2.7-1-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Switch to virtual debhelper-compat (= 11)
  * debian/compat: Removed

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 15 Oct 2020 17:44:06 -0500

rcmdr (2.7-0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 28 Aug 2020 08:06:10 -0500

rcmdr (2.6-2-2) unstable; urgency=medium

  * Rebuilt for r-4.0 transition

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 20 May 2020 14:04:46 -0500

rcmdr (2.6-2-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 25 Jan 2020 17:33:08 -0600

rcmdr (2.6-1-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 27 Nov 2019 15:20:26 -0600

rcmdr (2.6-0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Add r-cran-formatr to the ever expanding list of (Build-Depends)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 03 Sep 2019 07:11:18 -0500

rcmdr (2.5-3-2) unstable; urgency=medium

  * Source-only upload
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 17 Aug 2019 17:58:15 -0500

rcmdr (2.5-3-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 12 May 2019 16:59:23 -0500

rcmdr (2.5-2-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 12 Mar 2019 17:17:13 -0500

rcmdr (2.5-1-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 12 Sep 2018 19:33:53 -0500

rcmdr (2.5-0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

  * debian/control: Updated versioned (Build-)Depends: on r-cran-{car,rcmdrmisc}

  * debian/control: Corrected Architecture: to 'all'

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 23 Aug 2018 16:06:17 -0500

rcmdr (2.4-4-1) unstable; urgency=medium

  * New upstream release
    [ which lead to a full two-month wait for the required r-cran-rcmdrmisc
    version as several packages had to get through the NEW queue ]

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Build-Depends: to 'debhelper (>= 10)'
  * debian/control: Set Standards-Version: to current version 
  * debian/control: Add Vcs-Browser: and Vcs-Git:
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 08 Jun 2018 05:24:22 -0500

rcmdr (2.4-3-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Updated Build-Depends: to 'r-cran-rcmdrmisc (>= 1.0.10)'

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Switch from cdbs to dh-r
  * debian/rules: Idem

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 03 Apr 2018 08:28:32 -0500

rcmdr (2.4-2-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 01 Mar 2018 16:32:11 -0600

rcmdr (2.4-1-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 08 Nov 2017 08:51:14 -0600

rcmdr (2.4-0-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Updated (Build-)Depends: to match DESCRIPTION
  
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  * debian/compat: Increase level to 9

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 06 Nov 2017 19:53:44 -0600

rcmdr (2.3-2-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 03 Jan 2017 13:06:55 -0600

rcmdr (2.3-1-3) unstable; urgency=medium

  * debian/control: Make Depends and Build-Depends for r-cran-rgl exclude
    'armel' where r-cran-rgl is not building reliably 	(Closes: 846089)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 20 Dec 2016 06:39:23 -0600

rcmdr (2.3-1-2) unstable; urgency=low

  * debian/control: Make Build-Depends on r-cran-rgl exclude 'armel' where
    r-cran-rgl is not building reliably 		(Closes: 846089)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 28 Nov 2016 06:48:40 -0600

rcmdr (2.3-1-1) unstable; urgency=medium

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 27 Oct 2016 06:10:15 -0500

rcmdr (2.3-0-1) unstable; urgency=medium

  * New upstream release
  
  * debian/control: Added 'r-cran-markdown, r-cran-knitr,
    r-cran-rglwidget' to Suggests: 			(Closes: #831589)

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 25 Aug 2016 06:42:49 -0500

rcmdr (2.2-5-1) unstable; urgency=medium

  * New upstream release

  * debian/compat: Created
 
  * debian/control: Set Build-Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 22 Jun 2016 18:29:39 -0500

rcmdr (2.2-4-1) unstable; urgency=medium

  * New upstream release

  * debian/control: Add (Build-)Depends: on r-cran-relimp

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 04 Apr 2016 07:36:03 -0500

rcmdr (2.2-3-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 11 Nov 2015 06:24:07 -0600

rcmdr (2.2-2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 20 Oct 2015 08:03:37 -0500

rcmdr (2.2-1-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 18 Sep 2015 11:54:38 -0500

rcmdr (2.2-0-1) unstable; urgency=low

  * New upstream release
 
  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 19 Aug 2015 16:54:58 -0500

rcmdr (2.1-7-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 22 Feb 2015 17:22:48 -0600

rcmdr (2.1-6-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 09 Feb 2015 19:35:56 -0600

rcmdr (2.1-5-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Standards-Version: to current version 
  * debian/control: Set Build-Depends: to current R version
  
 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 10 Dec 2014 06:37:10 -0600

rcmdr (2.1-4-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 30 Oct 2014 19:17:31 -0500

rcmdr (2.1-3-1) unstable; urgency=low

  * New upstream release

  * The package has Build-Depends "markdown, knitr" which are not
    currently satisfiable within Debian, so the package was again lightly
    patched in three lines of R code to test for the package and suggest
    manual installation

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 09 Oct 2014 11:53:47 -0500

rcmdr (2.1-2-1) unstable; urgency=low

  * New upstream release

  * The package has Build-Depends "markdown, knitr" which are not
    currently satisfiable within Debian, so the package was again lightly
    patched in three lines of R code to test for the package and suggest
    manual installation

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 27 Sep 2014 06:50:16 -0500

rcmdr (2.1-1-1) unstable; urgency=low

  * New upstream release

  * The package has new Build-Depends "markdown, knitr" which are not
    currently satisfiable within Debian, so the package was again lightly
    patched in three lines of R code to test for the package and suggest
    manual installation
  
  * debian/control: Added Build-Depends: on r-cran-rcmdrmisc 

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 05 Sep 2014 16:54:38 -0500

rcmdr (2.1-0-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set Build-Depends: to current R version
  
  * The package has new Build-Depends "markdown, knitr" which are not
    currently satisfiable within Debian, so the package was lightly
    patched in three lines of R code to test for the package and suggest
    manual installation

  * The package has new Build-Depends for RcmdrMisc which is essentially
    an empty metapackage we added to Debian (in the NEW queue) and abind for
    which we added r-cran-abind
  * For now, 'RcmdrMisc (>= 1.0-1)' removed from Depends

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 24 Aug 2014 17:35:42 -0500

rcmdr (2.0-4-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 28 Mar 2014 06:39:56 -0500

rcmdr (2.0-3-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 16 Jan 2014 22:55:26 -0600

rcmdr (2.0-2-1) unstable; urgency=low

  * New upstream release (2.0-1 was skipped as it depended on packages not
    in Debian which has now been addressed by upstream -- thanks!!)

  * debian/control: Set Standards-Version: to current version 
  * debian/control: Added (Build-)Depends on r-cran-tcltk2 which is now in
    Debian as a new dependency

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 28 Dec 2013 11:45:00 -0600
  
rcmdr (2.0-0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 21 Aug 2013 15:17:26 -0500

rcmdr (1.9-6-2) unstable; urgency=low

  * debian/control: Set Build-Depends: to current R version 
  
  * (Re-)building with R 3.0.0 (beta)

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 30 Mar 2013 16:22:01 -0500

rcmdr (1.9-6-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 15 Mar 2013 06:20:20 -0500

rcmdr (1.9-5-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 08 Feb 2013 08:03:45 -0600

rcmdr (1.9-4-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 25 Jan 2013 13:36:37 -0600

rcmdr (1.9-3-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 16 Jan 2013 15:55:32 -0600

rcmdr (1.9-2-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Standards-Version: to current version 

  * debian/control: Updated (Build-)Depends: on r-cran-car (>= 2.0-15-1)
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 14 Oct 2012 11:52:08 -0500

rcmdr (1.9-1-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 18 Sep 2012 05:21:25 -0500

rcmdr (1.9-0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 01 Sep 2012 11:08:01 -0500

rcmdr (1.8-4-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set Build-Depends: to current R version
  * debian/control: Change Depends to ${R:Depends}
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 11 Apr 2012 15:41:51 -0500

rcmdr (1.8-3-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 17 Feb 2012 16:23:59 -0600

rcmdr (1.8-1-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  
 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 27 Dec 2011 07:38:10 -0600

rcmdr (1.7-3-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 20 Nov 2011 13:54:11 -0600

rcmdr (1.7-2-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 06 Nov 2011 18:10:52 -0600

rcmdr (1.7-0-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 04 Aug 2011 08:32:30 -0500

rcmdr (1.6-4-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 01 May 2011 06:42:24 -0500

rcmdr (1.6-3-1) unstable; urgency=low

  * New upstream release

  * debian/control: Added r-cran-hmisc to Depends
  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 03 Jan 2011 10:40:33 -0600

rcmdr (1.6-2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 03 Nov 2010 11:47:55 -0500

rcmdr (1.6-1-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 18 Oct 2010 09:09:14 -0500

rcmdr (1.6-0-2) unstable; urgency=low

  * Forcing rebuild under r-base-core (>= 2.11.1-5) to ensure working
    tcl/tk environment is found on mips and mipsel (Closes: #590126)
  * debian/control: Set Build-Depends: base-core (>= 2.11.1-5)
  * New code change, no other package changes

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 08 Aug 2010 11:27:55 -0500

rcmdr (1.6-0-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 28 Jul 2010 15:48:23 -0500

rcmdr (1.5-6-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 22 Jun 2010 19:19:28 -0500

rcmdr (1.5-5-1) unstable; urgency=low

  * New upstream release
  
  * debian/r-cran-rcmdr.install: Pick inst/etc/linux/Rcmdr.desktop 
    	 						(Closes: #562813)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 08 Jun 2010 15:58:12 -0500

rcmdr (1.5-4-2) unstable; urgency=low

  * debian/control: Added r-cran-car to Build-Depends: as Rcmdr now
    appears to have a hard rather than soft dependency	(Closes: #577365)

  * inst/etc/linux/Rcmdr.desktop: Include updated version also reported 
    upstream 						(Closes: #562813)

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 11 Apr 2010 10:30:00 -0500

rcmdr (1.5-4-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 23 Dec 2009 06:41:54 -0600

rcmdr (1.5-3-2) unstable; urgency=low

  * Rebuilt for R 2.10.0 to work with new R-internal help file conversion

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 03 Nov 2009 13:19:39 -0600

rcmdr (1.5-3-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 05 Oct 2009 14:58:49 -0500

rcmdr (1.5-2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 06 Sep 2009 13:00:00 -0500

rcmdr (1.5-1-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 30 Aug 2009 15:07:02 -0500

rcmdr (1.4-10-2) unstable; urgency=low

  * debian/rules: Set mode of Rcmdr/etc/linux/Rcmdr.sh to 0755  
  							(Closes: #525971)

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 28 Apr 2009 08:13:40 -0500

rcmdr (1.4-10-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 27 Apr 2009 16:26:39 -0500

rcmdr (1.4-9-1) unstable; urgency=low

  * New upstream release

  * debian/control: Changed Section: to new section 'gnu-r'
  
  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 3.8.1

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 04 Apr 2009 20:40:13 -0500

rcmdr (1.4-8-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 13 Mar 2009 13:43:57 -0500

rcmdr (1.4-7.1-1) unstable; urgency=low

  * Slightly modified new upstream release with three new contributed files 
    in inst/etc/linux to provide a desktop file for Rcmdr as suggested 
    by Pietro Battiston; extra thanks for his creation of the png file.
    All three files should be in the next upstream release.

  * debian/r-cran-rcmdr.{install,desktop}: Added provide desktop file 
  							(Closes: #518596)
  
 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 07 Mar 2009 15:33:52 -0600

rcmdr (1.4-7-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 08 Jan 2009 08:28:27 -0600

rcmdr (1.4-6-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 21 Dec 2008 14:09:55 -0600

rcmdr (1.4-5-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 15 Nov 2008 15:28:34 -0600

rcmdr (1.4-4-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 28 Oct 2008 20:50:32 -0500

rcmdr (1.4-3-1) unstable; urgency=low

  * New upstream release
  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 25 Oct 2008 08:40:31 -0500

rcmdr (1.4-2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 22 Sep 2008 20:45:54 -0500

rcmdr (1.4-1-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 16 Sep 2008 08:36:58 -0500

rcmdr (1.4-0-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 08 Aug 2008 11:00:30 -0500

rcmdr (1.3-15-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 08 Jun 2008 15:38:29 -0500

rcmdr (1.3-14-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 10 Apr 2008 16:33:09 -0500

rcmdr (1.3-13-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 06 Apr 2008 09:27:26 -0500

rcmdr (1.3-12-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 24 Jan 2008 22:38:51 -0600

rcmdr (1.3-11-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 13 Jan 2008 20:20:02 -0600

rcmdr (1.3-10-1) unstable; urgency=low

  * New upstream release

  * debian/control: Set (Build-)Depends: to current R version
  * debian/control: Set Standards-Version: to current version 3.7.3
  
 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 31 Dec 2007 18:11:56 -0600

rcmdr (1.3-9-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 20 Nov 2007 19:56:03 -0600

rcmdr (1.3-8-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 07 Nov 2007 21:06:09 -0600

rcmdr (1.3-6-1) unstable; urgency=low

  * New upstream release

  * Built with R 2.6.0, so setting (Build-)Depends: to 
    'r-base-(core|dev) >= 2.6.0' to prevent move to testing before R 2.6.0
  
 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 05 Nov 2007 21:29:58 -0600

rcmdr (1.3-5-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 30 Jul 2007 20:05:09 -0500

rcmdr (1.3-4-1) unstable; urgency=low

  * New upstream release
  * debian/control: (Build-)Depends: updated to r-base-(dev|core) (>= 2.5.1)

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 07 Jul 2007 21:02:05 -0500

rcmdr (1.3-3-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Build-Depends: updated to r-base-dev (>= 2.5.1~20070622)

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 24 Jun 2007 21:43:05 -0500

rcmdr (1.3-2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 14 Jun 2007 21:02:41 -0500

rcmdr (1.3-1-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 11 Jun 2007 20:25:16 -0500

rcmdr (1.3-0-1) unstable; urgency=low

  * New upstream release

  * debian/control: Build-Depends: updated to r-base-dev (>= 2.5.1~20070513)
  
 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 29 May 2007 20:36:07 -0500

rcmdr (1.2-9-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Build-Depends: updated to r-base-dev (>= 2.5.0~20070422)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 23 Apr 2007 15:51:40 -0500

rcmdr (1.2-7-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 18 Jan 2007 22:03:30 -0600

rcmdr (1.2-6-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 21 Dec 2006 22:00:31 -0600

rcmdr (1.2-5-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed,  6 Dec 2006 23:13:04 -0600

rcmdr (1.2-4-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 28 Nov 2006 21:25:41 -0600

rcmdr (1.2-3-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 22 Nov 2006 20:45:22 -0600

rcmdr (1.2-2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun,  8 Oct 2006 20:55:09 -0500

rcmdr (1.2-1-1) unstable; urgency=low

  * New upstream release, built using the R 2.4.0 releases candidate

  * debian/control: Upgraded Build-Depends: to 'r-base-dev (>> 2.3.1)' 
    and Depends: to 'r-base-core (>> 2.3.1)' accordingly

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 24 Sep 2006 10:20:06 -0500

rcmdr (1.2-0-1) unstable; urgency=low

  * New upstream release

  * debian/rules: Simplified to cdbs-based one-liner sourcing r-cran.mk 
  * debian/control: Hence Build-Depends: updated to r-base-dev (>= 2.3.1)

  * debian/control: Standards-Version: increased to 3.7.2

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 30 Aug 2006 20:03:32 -0500

rcmdr (1.1-7-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 26 Feb 2006 20:09:57 -0600

rcmdr (1.1-6-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon,  6 Feb 2006 21:04:43 -0600

rcmdr (1.1-5-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 11 Jan 2006 12:08:51 -0600

rcmdr (1.1-4-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue,  6 Dec 2005 21:26:38 -0600

rcmdr (1.1-2-1) unstable; urgency=low

  * New upstream release
  * debian/post{inst,rm}: No longer call R to update html help index
  * debian/watch: Updated regular expression

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 18 Nov 2005 20:17:54 -0600

rcmdr (1.1-1-1) unstable; urgency=low

  * New upstream release
  * debian/watch: Corrected regular expression (thanks, Rafael Laboissier)
  * debian/post{inst,rm}: Call /usr/bin/R explicitly (thanks, Kurt Hornik)
  * debian/control: Upgraded Standards-Version: to 3.6.2.1

 -- Dirk Eddelbuettel <edd@debian.org>  Thu,  8 Sep 2005 23:47:10 -0500

rcmdr (1.0-2-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 30 May 2005 20:15:18 -0500

rcmdr (1.0-1-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 28 Apr 2005 20:24:45 -0500

rcmdr (1.0-0-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 19 Apr 2005 22:18:19 -0500

rcmdr (0.9.17-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 18 Jan 2005 20:34:28 -0600

rcmdr (0.9.16-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 14 Dec 2004 21:02:01 -0600

rcmdr (0.9.15-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Fri, 10 Dec 2004 20:23:34 -0600

rcmdr (0.9.14-2) unstable; urgency=low

  * debian/control: Ooops, remove 2nd relimp Depends as we already had one
  * debian/rules: Set mode of pdf doc to 0644 (thanks, Lintian)

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 29 Nov 2004 22:11:19 -0600

rcmdr (0.9.14-1) unstable; urgency=low

  * New upstream release
  * debian/control: Added r-cran-relimp to Depends: following DESCRIPTION

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 29 Nov 2004 21:34:30 -0600

rcmdr (0.9.12-2) unstable; urgency=low

  * debian/control: Typo in versioned Depends on r-cran-sm (Closes: #279978)

 -- Dirk Eddelbuettel <edd@debian.org>  Sat,  6 Nov 2004 08:01:49 -0600

rcmdr (0.9.12-1) unstable; urgency=low

  * New upstream release

  * The new releases added a dependency on the strucchange package:
  * debian/control: Added r-cran-strucchange (which implies r-cran-sandwich
    and r-cran-zoo) to Depends as all three are now part of Debian
    (which are now in the archive) to Depends

  * debian/control: Made Depends: versioned to ensure packages rebuilt 
    under R 2.0.0 are select
  
 -- Dirk Eddelbuettel <edd@debian.org>  Wed,  3 Nov 2004 19:48:32 -0600

rcmdr (0.9.11-2) unstable; urgency=low

  * Rebuilt under R 2.0.0
  * debian/control: Updated Build-Depends: and Depends: accordingly
  * debian/post{inst,rm}: Only run build-help.pl if R is installed

 -- Dirk Eddelbuettel <edd@debian.org>  Fri,  8 Oct 2004 18:21:48 -0500

rcmdr (0.9.11-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 17 Aug 2004 21:29:25 -0500

rcmdr (0.9.10-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Wed,  4 Aug 2004 21:41:11 -0500

rcmdr (0.9.9-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Thu, 24 Jun 2004 16:53:53 -0500

rcmdr (0.9.8-1) unstable; urgency=low

  * New upstream release
  * debian/control: Added r-cran-rgl, r-cran-sm to Depends (Closes: #248429)
  * debian/post{inst,rm}: Call build-help.pl to update package.html list

 -- Dirk Eddelbuettel <edd@debian.org>  Tue, 25 May 2004 21:05:32 -0500

rcmdr (0.9.6-1) unstable; urgency=low

  * New upstream release
  
  * debian/control: Added 'r-cran-multcomp, r-cran-mvtnorm, r-cran-relimp'
    to Depends as these packages are now in the archive. Also added the
    (upstream recommended) r-cran-mgcv which is used with r-cran-rgl for
    three-dimensional scatterplots
  * debian/control: Added 'r-cran-rgl' to Suggests: as that package is 
    still stuck in Debian's incoming queue

 -- Dirk Eddelbuettel <edd@debian.org>  Sun, 21 Mar 2004 18:23:25 -0600

rcmdr (0.9.4-1) unstable; urgency=low

  * New upstream release
  
  * Upstream continues to depend on more external CRAN packages not all
    of which are currently packaged which means that some added
    functionality may not yet be available in the Debian package.
    As of this version, mgcv and Rgl have been added for 3d-scatterplot
    yet Rgl has not been packaged (but is expected to hit CRAN 'soon')
  
  * debian/control: Added Depends on r-cran-lmtest

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 16 Feb 2004 23:47:47 -0600

rcmdr (0.9.3-1) unstable; urgency=low

  * New upstream release

 -- Dirk Eddelbuettel <edd@debian.org>  Sun,  4 Jan 2004 16:32:01 -0600

rcmdr (0.9.2-2) unstable; urgency=low

  * debian/control: Added Depends on r-cran-{abind,effects} 

 -- Dirk Eddelbuettel <edd@debian.org>  Mon, 29 Dec 2003 18:36:17 -0600

rcmdr (0.9.2-1) unstable; urgency=low

  * Upgraded to new upstream release
  * debian/rules: Minor update moving towards common cdbs file

 -- Dirk Eddelbuettel <edd@debian.org>  Wed, 10 Dec 2003 20:59:44 -0600

rcmdr (0.9.0-1) unstable; urgency=low

  * Initial Debian Release

 -- Dirk Eddelbuettel <edd@debian.org>  Sat, 23 Aug 2003 12:53:24 -0500


